# README #

you must have docker and git installed on your machine.
### What is this repository for? ###

* testing docker, django, django rest framework, postgresql and celery

### How do I get set up? ###

* ```git init ```
* ```git remote add origin https://lagm1290@bitbucket.org/lagm1290/django-prueba.git ```
* ```git clone https://lagm1290@bitbucket.org/lagm1290/django-prueba.gits ```
* cd lss django-prueba
* ```docker-compose build ```
* ```docker-compose run web python manage.py makemigrations ```
* ```docker-compose run web python manage.py migrate```
* ```docker-compose run web python manage.py createsuperuser```
* ```docker-compose up```
* open browser http://localhost:8000/admin/
* add site
    1. localhost:8000&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;localhost:8000
    2. localhost:8000/activate&nbsp;&nbsp;activated
* open postman and import shop.postman_collection.json, user.postman_collection.json


